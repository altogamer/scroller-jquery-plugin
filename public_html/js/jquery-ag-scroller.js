/*  This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
(function($) {

    /** The amount of pixels to scroll. */
    var scrollIncrement = 1;

    $.fn.altogamerScroller = function(options) {
        var $scrollContainer = this;
        var offsetLeft = 0;
        var $items;
        var $manualScrollContainer;
        var settings = $.extend({
            height: "50px",
            width: "100px",
            speed: 50,
            itemClass: "ag-scroller-item",
            manualScrollClass: "ag-scroller-manual"
        }, options);

        $scrollContainer.css("height", settings.height);

        //prepare each item for scrolling
        $items = $scrollContainer.find("." + settings.itemClass);
        $items.each(function(i, item) {
            var $item = $(item);
            $item.css("position", "absolute");
            $item.css("width", settings.width);
            $item.css("height", settings.height);
            $item.css("left", offsetLeft + "px");
            offsetLeft += $item.width();
            loadImage($item);
        });

        //prepare manual scrolling
        $manualScrollContainer = $scrollContainer.find("." + settings.manualScrollClass);
        $manualScrollContainer.mouseover(function() {
            scrollIncrement = 6;
        });
        $manualScrollContainer.mouseout(function() {
            scrollIncrement = 1;
        });

        //display scroller and start scrolling
        $scrollContainer.slideDown();
        setInterval(function() {
            scroll($items);
        }, settings.speed);

        return this;
    };

    function loadImage($item) {
        findByKeywords({
            query: $item.data("keywords"),
            size: "medium",
            maxResults: 1
        }).done(function(data) {
            if (data.responseData.results[0]) {
                $item.css("background-image", "url('" + data.responseData.results[0].url + "')");
                $item.css("background-size", "cover");
            }
        });
    }


    function scroll($items) {
        var i;
        for (i = 0; i < $items.length; i++) {
            var item = $items[i];
            var width = parseInt(item.style.width, 10);
            var left = parseInt(item.style.left, 10) - scrollIncrement; //move item to the left by X pixels
            if (left <= -width) {
                // if item is offscreen, move it at the end of the line
                left = width * ($items.length - 1);
            }
            item.style.left = left + "px";
        }
    }




    /* Known domains that do not allow linking. */
    var BLACKLIST = [
        "1.bp.blogspot.com",
        "image.dosgamesarchive.com",
        "screens.latestscreens.com",
        "www.bluesnews.com",
        "www.gamesaktuell.de",
        "www.gamezone.de",
        "www.old-games.com",
        "www.pcgames.de",
        "www.pcgameshardware.com"
    ];

    /*
     * Google API documentation: https://developers.google.com/image-search/v1/jsondevguide?csw=1#json_args
     */
    function findByKeywords(options) {
        var settings = $.extend({
            query: "Bioshock",
            maxResults: undefined, //1-8
            size: undefined //icon, small, medium, large, xlarge, xxlarge, huge
        }, options);

        var url = "https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=" + encodeURIComponent(settings.query);
        url += settings.maxResults ? "&rsz=" + settings.maxResults : "";
        url += settings.size ? "&imgsz=" + settings.size : "";

        return $.ajax({
            url: url,
            type: "GET",
            dataType: "jsonp"
        }).then(function(data) {
            return filterByDomain(data, BLACKLIST);
        });
    }

    /** Filters an array of images, checking if the URL is blacklisted.
     * @return Array a new Array with the filtered results.
     */
    function filterByDomain(data, blacklist) {
        var i, j, image;
        var filteredResults = [];
        var images = data.responseData.results;
        for (i = 0; i < images.length; i++) {
            image = images[i];
            if (containsAny(image.url, blacklist) === -1) {
                filteredResults.push(image);
            }
        }
        data.responseData.results = filteredResults;
        return data;
    }

    /** Checks if any of the strings in an array is contained in the sepecified string.
     * @return int the index in the array that is contained in the string, -1 if no matches are found.
     */
    function containsAny(str, strArray) {
        for (var j = 0; j < strArray.length; j++) {
            if (str.indexOf(strArray[j]) !== -1)
                return j;
        }
        return -1;
    }


}(jQuery));
